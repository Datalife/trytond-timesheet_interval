# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from .timesheet import Timesheet, TeamTimesheet, TeamTimesheetWork


def register():
    Pool.register(
        TeamTimesheet,
        TeamTimesheetWork,
        module='timesheet_interval', type_='model',
        depends=['team_timesheet'])
    Pool.register(
        Timesheet,
        module='timesheet_interval', type_='model')

====
Team Timesheet manage
====

Imports::

    >>> import sys
    >>> import doctest
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools import create_team, get_team
    >>> from proteus import Model, Wizard
    >>> from datetime import timedelta, datetime
    >>> from dateutil.relativedelta import relativedelta

Install team_timesheet Module::

    >>> config = activate_modules(['timesheet_interval', 'team_timesheet'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)
    >>> today = datetime.today()
    >>> _ = create_team()
    >>> team = get_team()
    >>> Work = Model.get('timesheet.work')
    >>> work1 = Work(name='Work1')
    >>> work1.save()
    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts.save()
    >>> tts_work = tts.works.new()
    >>> tts_work.work=work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.start_time.date() == tts.date
    True
    >>> tts_work.end_time.date() == tts.date
    True
    >>> tts_work.start_time = datetime(today.year, today.month, today.day, 8, 0)
    >>> tts_work.end_time = datetime(today.year, today.month, today.day, 15, 0)
    >>> tts_work.hours.seconds // 3600
    7
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts_work.end_time == datetime(today.year, today.month, today.day, 16, 0)
    True
    >>> tts.save()
    >>> tts_work.planned_hours.total_seconds() // 3600
    8.0
    >>> work, = [w for w in tts.works]
    >>> create_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.create_timesheets', [work])
    >>> create_work_timesheets.form.creation_type = 'all_employees'
    >>> create_work_timesheets.execute('edit_timesheets')
    >>> tts.reload()
    >>> timesheet = tts.works[0].timesheets[0]
    >>> timesheet.start_time == tts_work.start_time
    True
    >>> timesheet.end_time == tts_work.end_time
    True
    >>> tts_work.start_time = None
    >>> tts_work.end_time = datetime(today.year, today.month, today.day, 9, 0)

Check start date and end time domains::

    >>> tts.start_time = datetime(2022, today.month, today.day, 8, 0)
    >>> tts.end_time = datetime(today.year, today.month, today.day, 9, 0)
    >>> tts.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Start time" in "Team Timesheet" is not valid according to its domain. - 
    >>> tts.start_time = datetime(today.year, today.month, today.day, 8, 0)
    >>> tts.save()
    >>> work.start_time = datetime(today.year, today.month, today.day, 7, 0)
    >>> work.end_time = tts.end_time
    >>> work.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Start time" in "TeamTimesheet - Work" is not valid according to its domain. - 
    >>> work.start_time = tts.start_time
    >>> work.end_time = datetime(today.year, today.month, today.day, 10, 0)
    >>> work.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "End time" in "TeamTimesheet - Work" is not valid according to its domain. - 
    >>> work.start_time = tts.start_time
    >>> work.end_time = tts.end_time
    >>> work.save()

Check start date and end date domains in work with a tts without start and end times::

    >>> tts.start_time = None
    >>> tts.end_time = None
    >>> tts.save()
    >>> work.reload()
    >>> work.start_time = datetime(today.year, today.month, today.day, 7, 0)
    >>> work.end_time = datetime(today.year, today.month, today.day, 10, 0)
    >>> work.save()
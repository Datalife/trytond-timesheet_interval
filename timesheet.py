# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import dateutil
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from datetime import datetime, timedelta, time
from trytond.pyson import Eval, If, Bool, Or
from trytond.transaction import Transaction


class ExportMixin(object):

    @classmethod
    def export_data(cls, records, fields_names):
        pool = Pool()
        Company = pool.get('company.company')

        res = super().export_data(records, fields_names)

        company = Company(Transaction().context['company'])
        lzone = dateutil.tz.gettz(company.timezone
            ) if company.timezone else dateutil.tz.tzutc()
        szone = dateutil.tz.tzutc()

        for item in res:
            for _id_field, _field in enumerate(item):
                if isinstance(_field, datetime):
                    item[_id_field] = _field.replace(tzinfo=szone
                        ).astimezone(lzone).replace(tzinfo=None)

        return res


class IntervalMixin(ExportMixin):
    _interval_date_field = 'date'

    date_lower = fields.Function(fields.DateTime('Date lower'),
        'get_date_lower')
    date_upper = fields.Function(fields.DateTime('Date upper'),
        'get_date_upper')
    start_time = fields.DateTime('Start time', format="%H:%M",
        domain=[
            If(Bool(Eval('start_time')), [
                ('start_time', '>=', Eval('date_lower')),
                ('start_time', '<', Eval('date_upper'))],
                []
            )],
        states={'required': Bool(Eval('end_time'))},
        depends=['start_time', 'date_lower', 'date_upper'])
    end_time = fields.DateTime('End time', format="%H:%M",
        domain=[
            If(Bool(Eval('end_time')),
                ('end_time', '>', Eval('start_time')),
                ()
            )],
        states={'required': Bool(Eval('start_time'))},
        depends=['end_time', 'start_time'])

    @property
    def interval_date(self):
        return getattr(self, self._interval_date_field)

    def get_date_lower(self, name=None):
        return datetime.combine(self.interval_date, time.min)

    def get_date_upper(self, name=None):
        return datetime.combine(self.interval_date, time.min
            ) + timedelta(days=1)


class IntervalDurationMixin(IntervalMixin):

    @fields.depends(methods=['_calc_hours_by_start_end_time'])
    def on_change_start_time(self):
        self._calc_hours_by_start_end_time()

    @fields.depends(methods=['_calc_hours_by_start_end_time'])
    def on_change_end_time(self):
        self._calc_hours_by_start_end_time()

    @fields.depends('start_time', 'end_time')
    def _calc_hours_by_start_end_time(self):
        if self.end_time and self.start_time:
            self.interval_duration = self.end_time - self.start_time
        elif self.start_time and self.interval_duration:
            self.end_time = self.start_time + self.interval_duration
        elif self.end_time and self.interval_duration:
            self.start_time = self.end_time + self.interval_duration
        else:
            self.interval_duration = timedelta(hours=0)

    @fields.depends('start_time', 'end_time', 'date_lower')
    def calc_start_end_date(self, new_date):
        self.date_lower = datetime.combine(new_date, time.min)
        self.date_upper = datetime.combine(
            new_date, time.min) + timedelta(days=1)
        if self.start_time:
            self.start_time = self.date_lower + (
                self.start_time - datetime.combine(
                    self.start_time, time.min))
            self.end_time = self.start_time + self.interval_duration


class Timesheet(IntervalDurationMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    @property
    def interval_duration(self):
        return self.duration

    @interval_duration.setter
    def interval_duration(self, value):
        self.duration = value

    @fields.depends('date', methods=['calc_start_end_date'])
    def on_change_date(self):
        if self.date:
            self.calc_start_end_date(self.date)

    @fields.depends('duration', 'start_time', 'end_time')
    def on_change_duration(self):
        if not self.duration:
            return
        if self.start_time:
            self.end_time = self.start_time + self.duration
        elif self.end_time:
            self.start_time = self.end_time - self.duration

    @fields.depends('duration')
    def _calc_hours_by_start_end_time(self):
        super()._calc_hours_by_start_end_time()

    @fields.depends('duration')
    def calc_start_end_date(self, new_date):
        super().calc_start_end_date(new_date)


class TeamTimesheet(IntervalMixin, ExportMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        clause = Or(Bool(Eval('works', None)), Eval('state') != 'draft')
        for field_name in ['start_time']:
            field = getattr(cls, field_name)
            field.states['readonly'] = clause
            field.depends.extend(['works', 'state'])

    @fields.depends('date')
    def on_change_date(self):
        if self.date:
            self.date_lower = self.get_date_lower()
            self.date_upper = self.get_date_upper()


class TeamTimesheetWork(IntervalDurationMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'

    date = fields.Function(fields.Date('Team Timesheet Date'),
        'get_date')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.start_time.states['readonly'] = (Eval('tts_state') != 'draft')
        cls.start_time.depends.append('tts_state')
        cls.end_time.states['readonly'] = (Eval('tts_state') != 'draft')
        cls.end_time.depends.append('tts_state')
        cls.start_time.domain.append(
            If(Bool(
                Eval('_parent_team_timesheet', {}).get('start_time', None)),
                ('start_time', '>=', Eval('_parent_team_timesheet', {}
                    ).get('start_time')),
                ()))
        cls.end_time.domain.append(
            If(Bool(
                Eval('_parent_team_timesheet', {}).get('end_time', None)),
                ('end_time', '<=', Eval('_parent_team_timesheet', {}
                    ).get('end_time')),
                ()))

    def get_date(self, name=None):
        return self.team_timesheet and self.team_timesheet.date

    @property
    def interval_duration(self):
        return self.hours

    @interval_duration.setter
    def interval_duration(self, value):
        self.hours = value

    @fields.depends('hours')
    def _calc_hours_by_start_end_time(self):
        super()._calc_hours_by_start_end_time()

    @fields.depends('hours')
    def calc_start_end_date(self, new_date):
        super().calc_start_end_date(new_date)

    @fields.depends('time_type', methods=['on_change_with_planned_hours'])
    def on_change_start_time(self):
        super().on_change_start_time()
        self.planned_hours = self.on_change_with_planned_hours()

    @fields.depends('time_type', methods=['on_change_with_planned_hours'])
    def on_change_end_time(self):
        super().on_change_end_time()
        self.planned_hours = self.on_change_with_planned_hours()

    @fields.depends('hours', 'start_time', 'end_time')
    def on_change_hours(self):
        hours = self.hours or timedelta(0)
        if self.start_time:
            self.end_time = self.start_time + hours
        elif self.end_time:
            self.start_time = self.end_time - hours

    def _get_timesheet(self, Timesheet, tts_employee, hours):
        res = super()._get_timesheet(Timesheet, tts_employee, hours)
        res.start_time = self.start_time
        res.end_time = self.end_time
        return res

    @fields.depends('team_timesheet', '_parent_team_timesheet.date')
    def on_change_team_timesheet(self):
        super(TeamTimesheetWork, self).on_change_team_timesheet()
        if self.team_timesheet and self.team_timesheet.date:
            self.date_lower = datetime.combine(
                self.team_timesheet.date,
                time.min)
            self.date_upper = datetime.combine(
                self.team_timesheet.date, time.min) + timedelta(days=1)
            self.start_time = self.date_lower
            self.end_time = self.date_lower
            self.hours = timedelta(0)

    @classmethod
    def validate(cls, records):
        for r in records:
            r.calc_start_end_date(r.team_timesheet.date)
        super(TeamTimesheetWork, cls).validate(records)

    def _update_date(self, date):
        super()._update_date(date)
        self.calc_start_end_date(date)
        for line in self.timesheets:
            line.calc_start_end_date(date)
